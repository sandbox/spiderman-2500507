<?php

/**
 * @file
 * certify_book.classes.inc php file
 * Classes for the Drupal module Certify Book.
 */

class CertifyWebform extends CertifyCondition {
  public $num_submissions; // Number of submissions a given user has made to this webform
  public $min_submissions; // Minimum number of submissions to "pass" this webform

  /**
   * Return form elements for node form
   */
  public static function certify_form($node) {
    // Collect webform nodes from webform module
    $webform_types = webform_variable_get('webform_node_types');
    $webform_nodes = node_load_multiple(array(), array('type' => $webform_types));

    foreach ($webform_nodes as $nid => $webform) {
      $options[$nid] = $webform->title;
    }

    // Webforms
    $form['selected_webforms'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Webforms to be part of this certificate'),
      '#options' => $options,
      '#default_value' => Certify::getConditionsNidsFromNode($node, 'webform'),
    );
    return $form;
  }

  /**
   * Process node form submit
   */
  public static function certify_insert($node) {
    foreach ($node->selected_webforms as $nid => $webform) {
      if ($nid == $webform) {
        $webform_node = node_load($webform);
        // @TODO Should probably be done using an API

        db_insert('certify_conditions')->fields(array(
          'cid' => $node->nid,
          'vid' => $node->vid,
          'condtype' => 'webform',
          'condnode' => $webform
        ))->execute();
      }
    }
  }

  /**
   * Handle deletion of certificate
   */
  public function certify_delete($node) {
    // @TODO purge booklog entries ONLY if this node is the last referencing the selected book id
  }

  /**
   * Render condition for view
   */
  public function certify_view($node) {
    global $user;

    $this->fillProgress($user->uid);
    return theme('certify_webform_view', array('node' => $node, 'condition' => $this));
  }

  /**
   * Header string for output
   */
  public static function getViewHeader() {
    return t("Webforms");
  }

  /**
   * Calculate progress data for this condition
   */
  public function fillProgress($uid = 0) {
    module_load_include('inc', 'webform', 'includes/webform.submissions');
    $node = $this->loadConditionNode();
    $subs = webform_get_submissions(array('uid' => $uid, 'nid' => $node->nid ));

    # TODO: make these configurable so multiple submissions are required to "complete" the condition
    $this->started = count($subs) >= 1 ? TRUE : FALSE;
    $this->completed = count($subs) >= 1 ? TRUE: FALSE;
    $this->gotpoints = count($subs);
    $this->gotpointscapped = count($subs);
    $this->totalpoints = count($subs);
    $this->requiredpoints = count($subs);
  }
}
